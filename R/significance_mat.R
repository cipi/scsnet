#' Significance matrix computation
#'
#' The function builds the significance matrix from a list of simulated networks/tables and the initial contigency table.
#' It implements the method describing in the appendix of Feugnet and al (2017).
#'
#'
#' @param list.sim.nets list of simulated networks (output of the sim_randomized_nets function).
#' @param ini.mat initial contingency matrix. Be carefull, the co-presence is calculated on the objects located in the matrix columns.
#' @param tresholds integer vector of significance thresholds.
#' @param type If type = 0 use the ranking method, if type = 1 use the value percent method.
#'
#' @return Significance matrix
#'
#' @references Feugnet A, Rossi F and Filet C (2017) Co-presence Analysis and Economic Patterns: Mediterranean Imports in the Celtic World. Front. Digit. Humanit. 4:1. doi: 10.3389/fdigh.2017.00001
#' @export
#'
#'
significance_mat <- function(list.sim.nets, ini.mat, thresholds=c(0.05), type=0){
  # Calcul de la matrice de coprésence initiale
  two.mode.net <- igraph::graph_from_incidence_matrix(ini.mat)
  two.mode.net.proj <- igraph::bipartite.projection(two.mode.net)
  copresence.mat.ini <- as.matrix(igraph::get.adjacency(two.mode.net.proj$proj2, attr="weight"))

  # Calcul de la matrice de coprésence de chaque réseau simulé
  list.copresence.mat <- lapply(list.sim.nets, copresence_mat_from_sim_net)

  if (type==0){
      # Version matrice
      # <- t(sapply(seq_along(list.copresence.mat[[1]]), function(x) unlist(lapply(list.copresence.mat, "[[", x))))
      # Version liste
      copres.collection <- lapply(seq_along(list.copresence.mat[[1]]), function(x) unlist(lapply(list.copresence.mat, "[[", x)))

      # Rank the initial number of copresence
      min.max.ranks <- t(mapply(get_x_min_max_ranks, copresence.mat.ini, copres.collection))

      maxSignifThresh <- max(thresholds)

      list.signif <- apply(min.max.ranks, MARGIN = 1, function(x){
        if(x['max'] <= maxSignifThresh){
          x['max']
        } else if (x['min'] >= 1 - maxSignifThresh){
          x['min']
        } else {
          0.5
        }
        })

      significance.mat = matrix(data=list.signif, nrow = nrow(copresence.mat.ini), ncol = ncol(copresence.mat.ini),
                          dimnames = list(rownames(copresence.mat.ini), colnames(copresence.mat.ini)))
      return(significance.mat)
  } else if (type==1){

    matrix.med <- apply(simplify2array(list.copresence.mat), MARGIN = c(1,2), median)

    score <- Reduce('+', (lapply(list.copresence.mat, function(each.mat){ each.mat == copresence.mat.ini })))*10000 / (length(list.copresence.mat)*10000+1) + 1/(length(list.copresence.mat)*10000+1)
    Signe <- sign(copresence.mat.ini - matrix.med)
    Signe[Signe==0] <- 1
    score.signed <- score * Signe
    return(score.signed)

  } else {
    warning("Type is not 0 or 1")
  }

}
