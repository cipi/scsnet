SCSNET
======

*S*ignificativity *C*o-presence from *S*imulated *Net*works is a package providing facilities to conduct a co-presence analysis on datasets such as archaeological finds or ecological systems. It makes available visual and statistical tools for detecting patterns in the association of certain objects.

The starting point is generally a two dimensional table, describing links between the objects of study. The interest lies in the network formalisation of these data by a two-mode network. This network point of view enables to highlight co-presences, and even to compute significance scores of co-presences. The simulation of random networks under certain properties constraints, allows to construct a distribution and then to compute significativity scores like the usual statistical tests.

The development of the package is based on [Feugnet A, Rossi F and Filet C (2017) Co-presence Analysis and Economic Patterns: Mediterranean Imports in the Celtic World](<https://www.frontiersin.org/articles/10.3389/fdigh.2017.00001/full>). The article shows an example of use of the statistical tools implemented in SCSNet. Here, the input is a data table, where the discovery sites are laid out in rows and the the artifacts in columns. The cell are filled in a binary way, 1 for discovery and 0 if not. In the 2-mode network view, artifacts and sites are nodes and the discovery of an object in a site is represented by a link between the corresponding nodes of network. The appendix A explains how the significance score is computed.

The simulation part rests on [Iorio, F., Bernardo-Faura, M., Gobbi, A., Cokelaer, T., Jurman, G., & Saez-Rodriguez, J. (2016). Efficient randomization of biological networks while preserving functional characterization of individual nodes.](<https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-016-1402-1>) and the [BiRewire](<https://www.bioconductor.org/packages/release/bioc/html/BiRewire.html>) package. BiRewire can fastly generate hundred of randomised bipartite networks while preserving characterisation of the nodes in the initial network, e.g. their degrees. One can finally compute co-presences significativity under the null generated distribution.

You can use **devtools** or **remotes** to install the package :

```r
devtools::install_gitlab('cipi/scsnet')
```

or

```r
remotes::install_gitlab('cipi/scsnet')
```




